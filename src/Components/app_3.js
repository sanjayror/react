import React from 'react';
import { Buttons, TYPES, SIZES } from './ButtonComponent'
import { Combobox } from './ComboComponent'
import { CheckBox } from './CheckboxComponent'

export class App extends React.Component {
    constructor(props) {
        super(props);
        var listCombo = [{ "Value": "1", "Text": "IT111" }, { "Value": "2", "Text": "Sales" }, { "Value": "3", "Text": "HR" }];

        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            ComboData: listCombo,
            name: "Sandeep"
        };
    }

    componentDidMount() {
        fetch('http://localhost:3000/items').then(res => res.json())
            .then((result) => {
                this.setState({
                    isLoaded: true,
                    items: result
                });
            },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items, ComboData, name } = this.state;
        const officers = [{ id: 1, name: 'Captain Piett' }, { id: 2, name: 'General Veers' }, { id: 3, name: 'Admiral Ozzel' }, { id: 4, name: 'Commander Jerjerrod' }];
        let officeId = officers.map(t => t.id);

        var pilots = [{ id: 10, name: "Poe Dameron", years: 14, }, { id: 2, name: "Temmin 'Snap' Wexley", years: 30, }, { id: 41, name: "Tallissan Lintra", years: 16, }, { id: 99, name: "Ello Asty", years: 22, }];
        var totalYears = pilots.reduce((acc, pilot) => pilot.years > 16 ? acc + pilot.years : acc + 0, 0);
        var pilots2 = [{ id: 1, name: "Wedge Antilles", faction: "Rebels", }, { id: 2, name: "Ciena Ree", faction: "Empire", }, { id: 3, name: "Iden Versio", faction: "Empire", }, { id: 4, name: "Thane Kyrell", faction: "Rebels", }];
        var rebels = pilots2.filter(t => t.faction === "Rebels" || t.faction === "Empire1");
        var rebels1 = pilots2.filter(t => t.id).map(t => t.id + t.id).reduce((acc, val) => acc + val, 0);

        //Blog, Functional Component.
        function Blog() {
            return (<div> Return Blog </div>);
        }

        //Welcome, Functional Component.
        function Welcome(props) {
            return <h1>Hello, {props.name}</h1>;
        }

        if (error) {
            return <div>Error : {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading.........</div>
        } else {
            return (
                <div>
                    <ul>
                        <Blog />
                        <Welcome name={name}></Welcome>
                    </ul>
                    <ul>
                        ID : {officeId}
                    </ul>
                    <ul>
                        TotalYears : {totalYears}
                    </ul>
                    <ul>
                        <Buttons
                            text='Submit'
                            type='submit'
                            onClick={() => { }}
                            buttonSize={SIZES.LARGE}
                            buttonType={TYPES.SUCCESS} />
                    </ul>
                    <ul>
                        <Combobox data={ComboData} text="-- Select --"></Combobox>
                    </ul>
                    <ul>
                        <CheckBox data={ComboData}></CheckBox>
                    </ul>
                    <ul>
                        {/* { JSON.stringify(items) } */}

                        {items.map(items => (<li key={items.id}> {items.name} - {items.price} </li>))}
                        {/* {items.map(a => (<li> {a.name} </li>))} */}
                    </ul>
                </div>
            );
        }
    }
}
export default App;