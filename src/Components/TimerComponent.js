import React from 'react';

export class Timer extends React.Component {
   constructor(props) {
      super(props);

      this.state = {
         time: null
      }

      setInterval(() => {
         this.setState({ time: new Date().toLocaleTimeString() });
      }, this.props.Interval);
   }

   render() {
      const { time } = this.state;
      return (
         <div>
            <h2> It is : {time} </h2>
         </div>
      )
   }
};

export default Timer
