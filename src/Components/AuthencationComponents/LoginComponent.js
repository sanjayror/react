import React from 'react';
import TextBox from '../TextBoxComponent';
import { Buttons, TYPES, SIZES } from '../ButtonComponent';

export class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            UserId: "",
            Password: ""
        }
    }

    SetUserId = () => {
        this.setState({ UserId: this.state.UserId });
    }

    SetSubmit = () => {
        this.setState({ UserId: "a" });
    }

    render() {
        return (
            <div>
                <table border="1">
                    <thead>
                        <tr>
                            <td> User Name </td>
                            <td> <TextBox onChange={this.state.SetUserId}></TextBox> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colSpan="2">
                                <Buttons text='Submit' type='Submit' onClick={this.SetSubmit} buttonSize={SIZES.LARGE} buttonType={TYPES.SUCCESS} />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Login;