import React from 'react';
import { Component } from 'react';
import { Onlybutton } from './Onlybutton';
import '../index.css';

export class Hello extends Component {
   render() {
      var i = 1;
      var myStyle = {
         fontSize: 100,
         color: 'Green'
      };

      return (
         <div>
            Welcome ! hello
            <Onlybutton></Onlybutton>
            <h1>Header</h1>
            <h2>Content</h2>
            <h1>{1 + 1}</h1>
            <h1>{i == 1 ? 'True!' : 'False'}</h1>
            {/* <h6 style={myStyle}> h1 </h6> */}
            <p data-myattribute="somevalue">This is the content!!!</p>
            { /*Multi line comment...*/}
         </div>
      );
   }
}
export default Hello;


