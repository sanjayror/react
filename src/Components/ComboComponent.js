import React from 'react';

export const Combobox = ({ data, text }) => (
    <div>
        <select>
            <option>{text}</option>
            {data.map(items => (<option key={items.Value}> {items.Text} </option>))}
        </select>

        {/* <select>
            <option>{text}</option>
            <option>IT</option>
            <option>Sales</option>
            <option>HR</option>
        </select> */}
    </div>
);

export default Combobox
