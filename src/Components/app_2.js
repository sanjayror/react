import React from 'react';
import ReactDOM from 'react-dom';

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: '',
            isToggleOn: true,
            name: 'Josh Perez'
        };

        const name = 'Josh Perez';
        const _element = <h1>Hello, {name}</h1>;

        // This binding is necessary to make `this` work in the callback.
        this.updateState = this.updateState.bind(this);
        this.clearInput = this.clearInput.bind(this);
        // this.handleClick = this.handleClick.bind(this);
    };

    updateState(e) {
        this.setState({ data: e.target.value });
    };

    clearInput() {
        this.setState({ data: '' });
        ReactDOM.findDOMNode(this.refs.myInput).focus();
    }

    handleClick() {
        // throw new Error();
        
        this.setState((toggle) => ({
            isToggleOn: !toggle.isToggleOn
        }));
    }

    // This syntax ensures `this` is bound within handleClick.
    // Warning: this is *experimental* syntax.
    hangleClicks = () => {
        console.log("clicks");
    }

    componentDidCatch(error, info) {
        return (
            <div>
                <h1>There is an error accurred.</h1>
            </div>
        );
    }

    render() {
        return (
            <div>
                <input value={this.state.data} onChange={this.updateState} ref="myInput"></input>
                <button onClick={this.clearInput}>CLEAR</button>
                {/* <button onClick={this.hangleClick}>{this.state.isToggleOn ? 'ON' : 'OFF'}</button> */}
                <button onClick={(e) => this.handleClick(e)}>{this.state.isToggleOn ? 'ON' : 'OFF'}</button>
                <h4>{this.state.data}</h4>
            </div>
        );
    }
}
export default App;
