import React from 'react';
import Timer from './TimerComponent';
import Login from './AuthencationComponents/LoginComponent'

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: 'Initial data...',
            value: 0,
        }

        this.updateState = this.updateState.bind(this);
        this.Increament = this.Increament.bind(this);
    }

    Increament = () => {
        this.setState({ value: this.state.value + 1 });
    }

    updateState() {
        this.setState({ data: 'Data updated from the child component...' });
    }

    render() {
        const { data, value } = this.state;

        return (
            <div>
                <Content myDataProp={data} updateStateProp={this.updateState} fnIncreament={() => this.Increament()}> </Content>
                <h4>Increament : {value}</h4>
                <Timer Interval="1000"></Timer>
            </div>
        )
    }
}

export class Content extends React.Component {
    render() {
        return (
            <div>
                <button onClick={this.props.updateStateProp}>Click</button>
                <button onClick={this.props.fnIncreament}>Increament</button>
                <h3>{this.props.myDataProp}</h3>

                <Login></Login>
            </div>
        );
    }
}

export default App;