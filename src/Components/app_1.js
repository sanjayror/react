import React from 'react';
import { Hello } from './Hello'
import '../index.css';

export class App extends React.Component {
   constructor(props) {
      console.log('App : Constructor Calling......');

      super(props);

      this.state = {
         data: [
            { "id": 1, "name": "Foo", "age": "20" },
            { "id": 2, "name": "Bar", "age": "30" },
            { "id": 3, "name": "Baz", "age": "40" }
         ],
         datas: 'Initial data...',
         header: "Header from state...",
         content: "Content from state..."
      }

      this.updateState = this.updateState.bind(this);
   };

   componentWillMount() {
      console.log('App : componentWillMount');
   }

   componentDidMount() {
      console.log('App : componentDidMount');
   }

   componentWillUnmount() {
      console.log('App : componentWillUnmount');
   }

   updateState() {
      this.setState({ datas: 'Data updated...' })
   }

   render() {
      return (
         <div>
            {/* <Hello></Hello> */}
            {/* <Header /> */}
            <table>
               <tbody>
                  {this.state.data.map((person, i) => <TableRow key={i} data={person} />)}
               </tbody>
            </table>
            <div>
               <h1>{this.state.header}</h1>
               <h2>{this.state.content}</h2>
            </div>
            <div>
               <Header headerProp={this.state.header} />
               <Content contentProp={this.state.content} />
            </div>
            <div>
               <button onClick={this.updateState}>Click</button>
               <h4>{this.state.datas}</h4>
            </div>
         </div>
      );
   }
}
export class Header extends React.Component {
   componentWillMount() {
      console.log('Header : componentWillMount');
   }

   componentDidMount() {
      console.log('Header : componentDidMount');
   }

   componentWillUnmount() {
      console.log('Header : componentWillUnmount');
   }

   render() {
      return (
         <div>
            <h1>Header</h1>
         </div>
      )
   }
}
export class Content extends React.Component {
   componentWillMount() {
      console.log('Content : componentWillMount');
   }

   componentDidMount() {
      console.log('Content : componentDidMount');
   }

   componentWillUnmount() {
      console.log('Content : componentWillUnmount');
   }

   render() {
      return (
         <div>
            <h1>Content</h1>
            <p>The Content Text !!!</p>
         </div>
      )
   }
}
export class TableRow extends React.Component {
   componentWillMount() {
      console.log('TableRow : componentWillMount');
   }

   componentDidMount() {
      console.log('TableRow : componentDidMount');
   }

   componentWillUnmount() {
      console.log('TableRow : componentWillUnmount');
   }

   render() {
      return (
         <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.name}</td>
            <td>{this.props.data.age}</td>
         </tr>
      );
   }
}
export default App;
