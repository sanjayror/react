import React from 'react';

export const TYPES = {
   PRIMARY: 'primary',
   WARNING: 'warning',
   DANGER: 'danger',
   SUCCESS: 'success',
}

export const SIZES = {
   SMALL: 'small',
   MEDIUM: 'medium',
   LARGE: 'large'
}

export const Buttons = ({ text, onClick, type, disabled }) => (
   <button
      type={type}
      disabled={disabled}
      onClick={onClick}> {text}
   </button>
);

export default Buttons