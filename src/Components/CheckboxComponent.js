import React from 'react';

export const CheckBox = ({ data }) => {
    return (
        <div>
            {data.map((option, index) => {
                return (
                    <div key={option.Value} className="column">
                        <label>{option.Text}</label>
                        <input type="checkbox" value={option.Text} />
                    </div>
                );
            })}
        </div>
    );
};

export default CheckBox
